# Studio Classroom Interview Project (Front-end Developer)

空中英語教室 前端工程師面試試題


## 說明

請使用所提供之檔案，按照下列規格製作一份網頁應用程式：

1. 使用瀏覽器一開始載入 `index.html` 網頁時，User Info 清單與 User Table 表格中皆不會出現任何資料。

2. 按下「Fetch」按鈕，將會執行 HTTP 請求至 https://jsonplaceholder.typicode.com/users 抓取資料。接著按照 User List 表格中的欄位樣式，將 id 以大至小 (降冪) 的方式呈現所有資料。

3. 再按下「Parse」按鈕，將會使表格中 Username 的資料全部改為小寫字母，並將所有 Email 的資料加上連結。點選 Email 連結時，以另開新視窗的方式開啟系統預設郵件服務。
 
4. 按下每一筆資料的「Detail」按鈕，將會在上方的 User Info 清單中顯示相對應的使用者資訊。若 Phone 的資料開頭第一個字為 "1"，則整段資料以紅色字體醒目標示。

5. 按下每一筆資料的「Delete」按鈕，將會在表格中刪除該筆資料。

製作完成後，請按照指定方式繳交檔案。

### 備註

- 此專案可以使用任何函式庫或框架來完成。
- 製作過程中，可以任意搜尋相關文件，但請勿與他人討論或分享。